import httpAdapter from 'axios/lib/adapters/http';

const batchAdapter = () => {

  let cacheRequest = {};

  return (config) => {

    if(config.isNewBatch) {
      return new Promise((resolve, reject) => {
        httpAdapter(config).then(response => {
          const data = JSON.parse(response.data);
          //Asuming response data will only has items property and is an array
          //A more extensive check to be done if it's not the case
          if(!data.items.length) {
            return reject('Empty response');
          }
          //Cache request response
          cacheRequest = {...cacheRequest, ...response};
          return resolve(response);
        }).catch((e) => reject(e));
      });

    } else {

      return new Promise((resolve, reject) => {
        //Interval to wait for the first call response
        let timer = 0;

        const response = setInterval(() => {
          if(Object.values(cacheRequest).length) {
            
            //resolve with cache response
            let data = JSON.parse(cacheRequest.data);
            data.items = data.items.filter(item => config.params.ids.includes(item.id));
            
            if(!data.items.length) {
              clearInterval(response);
              return reject('Empty response');
            }

            data = JSON.stringify(data);
            clearInterval(response);
            return resolve({...cacheRequest, data});

          } else {

            timer++;
            
            if(timer === config.timeout) {
              return reject('Timeout');
            }

          }
        }, 1);
      });
    }
  }
}

export { batchAdapter };