import axios from 'axios';
import {batchInterceptor, emptyResponseInterceptor} from './interceptor';
import {batchAdapter} from './batchAdapter';

const adapter = batchAdapter();

const apiClient = () => { 
  
  const config = {
    adapter,
    baseURL: 'https://europe-west1-quickstart-1573558070219.cloudfunctions.net/',
    headers: {
      get: {
        'Content-Type': 'multipart/mixed;'
      }
    }
  };

  const instance = axios.create(config);

  batchInterceptor(instance);

  return instance;
}

export default apiClient();