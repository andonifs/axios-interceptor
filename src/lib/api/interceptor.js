import axios from 'axios';

function batchInterceptor(instance) { 

  let params = [];

  instance.interceptors.request.use(request => {

    if(request.params) {

      const ids = request.params.ids;

      if(ids) {

        if(!params.length) {
          params = [...ids];
          return {
            ...request,
            isNewBatch: true
          };
        }
    
        const isNewBatch = ids.some(id => {
          return params.indexOf(id) < 0;
        });
        
        params = [...params, ...ids];

        return {
          ...request,
          isNewBatch
        };
      }

      return request;
    }
    
    return request;
  }, error => Promise.reject);
}

export { batchInterceptor };