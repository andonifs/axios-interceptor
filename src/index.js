import apiClient from './lib/api/apiClient';

/* Test api client */

function runTest() {
  const batchUrl = '/file-batch-api';
  // Should batch in to one request
  apiClient.get(batchUrl, {params: {ids: ['fileid1','fileid2','fileid5','fileid4']}})
  .then(resp => console.log('1, 2, 4, 5 --> ', resp.data.items)); 

  apiClient.get(batchUrl, {params: {ids: ['fileid1']}})
  .then(resp => console.log('1 --> ', resp.data.items)); 

  apiClient.get(batchUrl, {params: {ids: ['fileid4', 'fileid2']}})
  .then(resp => console.log('2, 4 --> ', resp.data.items)); 

  apiClient.get(batchUrl, {params: {ids: ['fileid2', 'fileid5']}})
  .then(resp => console.log('2, 5 --> ', resp.data.items)); 

  apiClient.get(batchUrl, {params: {ids: ['fileid1', 'fileid2', 'fileid4']}})
  .then(resp => console.log('1, 2, 4 --> ', resp.data.items)); 

  // apiClient.get(batchUrl, {params: {ids: ['fileid1', 'fileid3', 'fileid4']}})
  // .then(resp => console.log('1, 4 --> ', resp.data.items)); 

  // The following should reject as the fileid3 is missing from theresponse
  apiClient.get(batchUrl, {params: {ids: ['fileid3']}});
} 

runTest();