# Batch API request axios interceptor

# Running

Test requires [Node.js](https://nodejs.org/) to run.

By default the App server runs in port _3300_.

First `npm install` to grab all the necessary dependencies.

**App dependencies**

```sh
$ cd client
$ npm install
```

**Run App**

```sh
$ cd client
$ npm run dev
```

**App Build**

```sh
$ cd client
$ npm run build
```

# Contact Email

andoni282@gmail.com
